import React, { Component } from 'react';
import { Card } from 'antd';

class RightMenuAdmin extends Component {
  render() {
    return (
      <div id="right-menu-main-wrapper">
        <Card className="card-style">
          <div>
            <div className="admin-setting-buttons">
              {/* <Button className="admin-circle1 shadow" shape="circle">
                <div className="setting-text-admin">
                  <RIcon size={32} icon={ic_settings} />
                  <p style={{ marginTop: '-3px' }}>Settings</p>
                </div>
              </Button> */}
            </div>
            <div className="admin-option-setting">
              <button
                onClick={() => this.props.handleClick(`/taccountant`)}
                className={
                  Number(localStorage.getItem('taccountant')) === 1
                    ? 'admin-option-items shadow'
                    : 'admin-option-items shadow hide'
                }
              >
                <p className="admin-options-text">T Accountant</p>
              </button>
              <button
                onClick={() => this.props.handleClick(`/Manage`)}
                className={
                  Number(localStorage.getItem('managecat')) === 1
                    ? 'admin-option-items shadow'
                    : 'admin-option-items shadow hide'
                }
              >
                <p className="admin-options-text">Manage</p>
              </button>
              <button
                onClick={() => this.props.handleClick(`/crm`)}
                className={
                  Number(localStorage.getItem('crm')) === 1
                    ? 'admin-option-items shadow'
                    : 'admin-option-items shadow hide'
                }
              >
                <p className="admin-options-text">CRM</p>
              </button>
              <button
                onClick={() => this.props.handleClick(`/hrm`)}
                className={
                  Number(localStorage.getItem('hrm')) === 1
                    ? 'admin-option-items shadow'
                    : 'admin-option-items shadow hide'
                }
              >
                <p className="admin-options-text">HRM</p>
              </button>
              <button
                onClick={() => this.props.handleClick(`/stock`)}
                className={
                  Number(localStorage.getItem('stock')) === 1
                    ? 'admin-option-items shadow'
                    : 'admin-option-items shadow hide'
                }
              >
                <p className="admin-options-text">Stock</p>
              </button>
              <button
                onClick={() => this.props.handleClick(`/unpaid`)}
                className={
                  Number(localStorage.getItem('invoice')) === 1
                    ? 'admin-option-items shadow'
                    : 'admin-option-items shadow hide'
                }
              >
                <p className="admin-options-text">Invoice</p>
              </button>
            </div>
          </div>
        </Card>
      </div>
    );
  }
}

export default RightMenuAdmin;
