import React, { Component } from 'react';
import { Button, Row, Col } from 'antd';
import './FloatingButtons.css';
import { push } from 'react-router-redux';
import PropTypes from 'prop-types';
import companyLogo from '../../assets/Aryan-Holding-Group-Logo-Square.png';

export default class FloatingButtons extends Component {
  static propTypes = {
    isBilling: PropTypes.bool
  };

  constructor() {
    super();
    this.state = {
      isActive: false
    };
  }

  toggleModal = () => {
    this.setState({
      isActive: !this.state.isActive
    });
  };

  render() {
    const website = localStorage.getItem('website');
    return (
      <Row gutter={12}>
        <div
          className="scrollbar"
          id={this.props.isBilling ? 'container-billing' : 'container'}
        >
          <Col>
            <div className="button-style1">
              <Button
                onClick={() => {
                  this.navigate('/admin');
                }}
                className="main-button button-size"
                shape="circle"
              >
                <img src={companyLogo} className="company-logo" />
              </Button>
            </div>
          </Col>
          <Col>
            <div className="button-style1">
              <Button
                className="open-ac-button  button-size"
                onClick={() => {
                  this.navigate('/plu');
                }}
                shape="circle"
              >
                <div className="button-text-style">
                  <span>Open Account</span>
                </div>
              </Button>
            </div>
          </Col>

          <Col>
            <div className="button-style1">
              <Button
                onClick={() => {
                  this.navigate('/tables');
                }}
                className="table-view-button button-size"
                shape="circle"
              >
                <div className="button-text-style">
                  <span>Table View</span>
                </div>
              </Button>
            </div>
          </Col>

          <Col>
            <div className="button-style1">
              <Button className="tfl-button  button-size" shape="circle">
                <div className="button-text-style">
                  <span>TFL</span>
                </div>
              </Button>
            </div>
          </Col>

          <Col>
            <div className="button-style1">
              <a href={website ? website : ''} target="_blank">
                <Button className="web-button  button-size" shape="circle">
                  <div className="button-text-style">
                    <span style={{ color: 'white' }}>Web</span>
                  </div>
                </Button>
              </a>
            </div>
          </Col>
        </div>
      </Row>
    );
  }
  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}
