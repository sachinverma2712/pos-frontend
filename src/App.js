import React, { Component } from 'react';
import './App.css';
import createHistory from 'history/createBrowserHistory';
import createStore from './reducers/create';
import ApiClient from './helpers/ApiClient';
import { Provider } from 'react-redux';
import Main from './Main';
const history = createHistory();
const client = new ApiClient();
const store = createStore(history, client, window.__INITIAL_STATE__);
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Main history={history} />
      </Provider>
    );
  }
}

export default App;
