export const apiUrl = 'http://139.59.14.213:3041';

export const reqHeaders = token => {
  return {
    Accept: 'application/json',
    'Content-type': 'application/json',
    authorization: `bearer ${token}`
  };
};
