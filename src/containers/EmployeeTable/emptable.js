import React, { Component, Fragment } from 'react';
import Home from '../Home/Home';
import { connect } from 'react-redux';
import {
  Table,
  Tag,
  Popover,
  Input,
  InputNumber,
  Form,
  Switch,
  notification,
  Card
} from 'antd';
import endpoint from '../../helpers/endpoint';
import Modal from 'react-modal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends Component {
  getInput = () => {
    if (this.props.inputType === 'number') {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (
      <EditableContext.Consumer>
        {form => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [
                      {
                        required: true,
                        message: `Please Input ${title}!`
                      }
                    ],
                    initialValue: record[dataIndex]
                  })(this.getInput())}
                </FormItem>
              ) : (
                restProps.children
              )}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}

class employeetable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      isAuthenticated: false,
      modalIsOpen: false,
      visiblePopover: false,
      deletingKey: '',
      currentPage: 'empTable',
      first_name: '',
      last_name: '',
      address: '',
      items: '',
      editingKey: '',
      currentEmp: '',
      permissions: [
        'taccountant',
        'managecat',
        'crm',
        'hrm',
        'stock',
        'invoice'
      ],
      taccountant: 0,
      managecat: 0,
      crm: 0,
      hrm: 0,
      stock: 0,
      invoice: 0
    };
    this.toggleModal = this.toggleModal.bind(this);

    this.empColumns = [
      {
        title: 'Name',
        width: '20%',
        editable: false,
        dataIndex: 'name',
        render: (item, record) => (
          <a
            style={{ color: '#1890ff' }}
            onClick={() => this.showEmployeeProfile(record)}
          >
            {item}
          </a>
        )
      },
      {
        title: 'Email',
        dataIndex: 'email',
        editable: false,
        width: '26%',
        render: (item, record) => (
          <a
            style={{ color: '#1890ff' }}
            onClick={() => this.showEmployeeProfile(record)}
          >
            {item}
          </a>
        )
      },
      {
        title: 'Accress Levels',
        editable: false,
        width: '30%',
        render: text => {
          let roles = [];
          text.hrm && roles.push(<Tag color="blue">HRM</Tag>);
          text.crm && roles.push(<Tag color="blue">CRM</Tag>);
          text.taccountant && roles.push(<Tag color="blue">T Accountant</Tag>);
          text.managecat && roles.push(<Tag color="blue">Manage</Tag>);
          text.invoice && roles.push(<Tag color="blue">Invoice</Tag>);
          text.stock && roles.push(<Tag color="blue">Stock</Tag>);
          return roles;
        }
      },
      {
        title: 'Edit',
        dataIndex: 'operation',
        width: '12%',
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <a
              style={{ color: '#1890ff' }}
              onClick={() => this.toggleModal(record)}
            >
              Edit
            </a>
          );
        }
      },
      {
        title: 'Delete',
        width: '12%',
        // key: 'action',
        render: (text, record) => {
          const deletable = this.isDeleting(record);
          return (
            <Fragment>
              {deletable ? (
                <Popover
                  content={
                    <span
                      style={{
                        display: 'flex',
                        justifyContent: 'space-around'
                      }}
                    >
                      <a style={{ color: '#1890ff' }} onClick={this.hideDelete}>
                        Cancel
                      </a>
                      <a
                        style={{ color: '#F44336' }}
                        onClick={() => this.delete(record)}
                      >
                        Delete
                      </a>
                    </span>
                  }
                  title="Are you Sure?"
                  trigger="click"
                  visible={this.state.visiblePopover}
                  onVisibleChange={this.handleVisibleChange}
                >
                  <a
                    onClick={() => this.setState({ popoverId: record.id })}
                    style={{ color: '#F44336' }}
                  >
                    Delete
                  </a>
                </Popover>
              ) : (
                <a
                  onClick={() => this.deleteTemp(record.email)}
                  style={{ color: '#F44336' }}
                >
                  Delete
                </a>
              )}
            </Fragment>
          );
        }
      }
      // {
      //   title: 'access',
      //   key: 'access',
      //   dataIndex: 'roles',
      //   render: tags => (
      //     <span>
      //       {tags.map(tag => (
      //         <Tag color="blue" key={tag}>
      //           {tag}
      //         </Tag>
      //       ))}
      //     </span>
      //   )
      // }
      // {
      //   title: 'Address',
      //   key: 'address',
      //   render: address =>
      //     `${address.line1}, ${address.line2}, ${address.city}, ${
      //       address.state
      //     }`
      // }
    ];
  }

  toggleModal(record) {
    console.log(record);
    const { crm, hrm, invoice, taccountant, managecat, stock } = record;
    this.setState({
      crm,
      hrm,
      taccountant,
      invoice,
      managecat,
      stock,
      modalIsOpen: !this.state.modalIsOpen,
      editingKey: record.email
    });
  }

  closeModal() {
    this.setState({ modalIsOpen: !this.state.modalIsOpen });
  }

  handleChange = e => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  };

  handleVisibleChange = () => {
    this.setState({ visiblePopover: true });
  };

  isEditing = record => {
    return record.id === this.state.editingKey;
  };

  isDeleting = record => {
    return record.email === this.state.deletingKey;
  };

  hideDelete = () => {
    this.setState({ deletingKey: '' });
  };

  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.items];
      const index = newData.findIndex(item => key === item.id);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row
        });

        this.setState(
          {
            items: newData,
            editingKey: ''
          },

          this.updateEmployee(
            row.email,
            row.hrm,
            row.crm,
            row.taccountant,
            row.invoice,
            row.stock,
            row.managecat,
            item._id
          )
        );
      } else {
        newData.push(row);
        this.setState({ items: newData, editingKey: '' });
      }
    });
  }

  delete = record => {
    const id = record.id;
    let hdr = localStorage.token;
    const data = JSON.stringify({ id });
    console.log(data);
    fetch(`${endpoint}/employee`, {
      method: 'DELETE',
      body: data,
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === 'failure') {
          console.log(result);
          this.openNotification('error', result.data);
          this.setState({ visiblePopover: false });
        } else {
          this.openNotification('success', result.message);
          console.log(result.message);
          this.showEmployees();
        }
      });
  };

  deleteTemp(key) {
    this.setState({ deletingKey: key });
  }

  changed = (checked, item) => {
    let toggledState = {};
    if (checked) {
      console.log('checked');
      toggledState[item] = 1;
    } else {
      console.log('unchecked');
      toggledState[item] = 0;
    }
    this.setState(toggledState);
    console.log(item);
  };

  editEmployee = e => {
    e.preventDefault();
    var data = JSON.stringify({
      email: this.state.editingKey,
      hrm: this.state.hrm,
      crm: this.state.crm,
      taccountant: this.state.taccountant,
      invoice: this.state.invoice,
      stock: this.state.stock,
      managecat: this.state.managecat
    });
    console.log(data);
    var hdr = localStorage.token;
    fetch(`${endpoint}/employee`, {
      method: 'PUT',
      body: data,
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(
        result => {
          console.log(result);
          if (result.status === 'failure') {
            console.log(result);
            this.openNotification(
              'error',
              result.data.errors ? result.data.errors[0].msg : result.data
            );
          } else {
            this.openNotification('success', result.message);
            // this.props.history.push('/hrm');
            console.log(result);
            this.closeModal();
            this.showEmployees();
            // window.location.reload();
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  cancel = () => {
    this.setState({ editingKey: '' });
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  goBack = () => {
    this.setState({ currentPage: 'empTable' });
  };

  showEmployees = () => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/employee`, {
      method: 'GET',
      headers: {
        'x-auth-token': hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            const employee = result.data.map(item => ({
              firstName: item.firstName,
              lastName: item.lastName,
              name: item.firstName + ' ' + item.lastName,
              email: item.email,
              address: item.address,
              id: item.id,
              phoneNo: item.phoneNo,
              personalInfo: item.personalInfo,
              // roles: [
              //   item.taccountant,
              //   item.managecat,
              //   item.crm,
              //   item.hrm,
              //   item.stock
              // ]
              hrm: item.hrm,
              crm: item.crm,
              taccountant: item.taccountant,
              stock: item.stock,
              invoice: item.invoice,
              managecat: item.managecat
            }));
            this.setState({
              isLoaded: true,
              items: employee
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  showEmployeeProfile = employee => {
    console.log(employee);
    this.setState({ currentPage: 'empProfile', currentEmp: employee });
  };

  componentDidMount() {
    this.showEmployees();
  }
  render() {
    console.log(this.state);
    const regex = /(^(.)|png|jpeg|jpg)$/;
    const permissionsAlt = {
      managecat: 'Manage',
      taccountant: 'T-Accountant',
      stock: 'Stock',
      crm: 'CRM',
      hrm: 'HRM',
      invoice: 'Invoice'
    };
    const {
      error,
      items,
      isLoaded,
      permissions,
      currentPage,
      currentEmp
    } = this.state;
    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell
      }
    };

    const empColumns = this.empColumns.map(col => {
      if (!col.editable) {
        return col;
      }

      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === 'price' ? 'number' : 'text',
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (currentPage === 'empTable') {
      return (
        <Home isAdmin={true}>
          <div className="report-container">
            <span className="item">HRM</span>
            <span className="item" />
          </div>
          <Table
            loading={isLoaded ? false : true}
            dataSource={items}
            columns={empColumns}
            rowKey={record => record.id}
            onChange={pagination => this.pageChange(pagination)}
            components={components}
            bordered
            rowClassName="editable-row"
          />

          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.toggleModal}
            style={customStyles}
            ariaHideApp={false}
            contentLabel="Add Employee"
          >
            <div className="button-container">
              <button onClick={this.toggleModal} className="close-button">
                X
              </button>
            </div>
            <form className="add-employee" onSubmit={e => this.editEmployee(e)}>
              <li>
                <center>
                  <h2>Add Employee</h2>
                </center>
              </li>
              <li>
                <input
                  type="text"
                  onChange={this.handleChange}
                  name="email"
                  className="input2"
                  placeholder="E-mail"
                  value={this.state.editingKey}
                  disabled
                />
              </li>
              <li className="designation-title">Employee Permissions</li>
              <li className="switches">
                {permissions.map(item => {
                  return (
                    <Switch
                      checkedChildren={permissionsAlt[item]}
                      unCheckedChildren={permissionsAlt[item]}
                      checked={this.state[item]}
                      onChange={checked => this.changed(checked, item)}
                    />
                  );
                })}
              </li>
              <li>
                <button type="submit" id="submit-landing2">
                  Submit
                </button>
              </li>
            </form>
          </Modal>
        </Home>
      );
    } else if (currentPage === 'empProfile') {
      return (
        <Home>
          <div style={{ maxWidth: 600, margin: '0 auto' }}>
            <Card
              title="HRM/User Profile"
              extra={<a onClick={() => this.goBack()}>Back</a>}
            >
              <div
                style={{
                  display: 'flex'
                }}
                className="hrm-profile-single"
              >
                <p style={{ fontWeight: 700, minWidth: 150 }}>Name</p>
                <p>{currentEmp.name}</p>
              </div>
              <div style={{ display: 'flex' }}>
                <p style={{ fontWeight: 700, minWidth: 150 }}>Email</p>
                <p>{currentEmp.email}</p>
              </div>
              <div style={{ display: 'flex' }}>
                <p style={{ fontWeight: 700, minWidth: 150 }}>Phone No.</p>
                <p>{currentEmp.phoneNo}</p>
              </div>
              <div style={{ display: 'flex' }}>
                <p style={{ fontWeight: 700, minWidth: 150 }}>Address</p>
                <p>
                  {currentEmp.address.line1} {currentEmp.address.line2}
                  <br />
                  {currentEmp.address.city} {currentEmp.address.pin}
                  <br />
                  {currentEmp.address.state} {currentEmp.address.country}
                </p>
              </div>
              <div>
                <span>
                  <h3>Documents</h3>
                </span>
                {currentEmp.personalInfo && currentEmp.personalInfo.length > 0
                  ? currentEmp.personalInfo.map((item, index) => {
                      if (item.match(regex)) {
                        return (
                          <div>
                            Document {index + 1}:
                            <img
                              style={{ maxWidth: '100%' }}
                              src={`${endpoint}/${item}`}
                              key={item}
                            />
                          </div>
                        );
                      } else {
                        return (
                          <div key={item}>
                            <a href={`${endpoint}/${item}`}>
                              Document {index + 1}
                            </a>
                            <br />
                          </div>
                        );
                      }
                    })
                  : ''}
              </div>
            </Card>
          </div>
        </Home>
      );
    }
  }
}
const mapStateToProps = state => {
  return {};
};
const emptab = connect(mapStateToProps)(employeetable);
export default emptab;
