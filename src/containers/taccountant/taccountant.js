import React, { Component } from 'react';
import ReactToPrint from 'react-to-print';
import Home from '../Home/Home';
import endpoint from '../../helpers/endpoint';
import './taccountant.css';
import { connect } from 'react-redux';
import { DatePicker, notification } from 'antd';
import TableSum from '../TableSum/TableSum';
import PrintReport from '../PrintReport/PrintReport';
import moment from 'moment';

class taccountantComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      sendProps: false,
      currentPage: 'buttons',
      items: [],
      tax: '',
      paginationNo: 1,
      unitPriceTotal: 0,
      total15Tax: 0,
      total25Tax: 0,
      total15Quantity: 0,
      total25Quantity: 0,
      finalTotal: 0,
      totalSubtotal: 0,
      totalTax: 0,
      totalTaxPrice: 0,
      totalDiscount: 0
    };
  }

  componentDidMount = () => {
    let date = moment().date()._d;
    this.loadReports(date);
  };

  handlePageChange = page => {
    this.setState({ currentPage: page });
  };

  pageChange = pagination => {
    if (pagination) {
      this.setState({ paginationNo: pagination.current });
    }
  };

  loadReports = date => {
    this.setState({ today: date });
    var data = JSON.stringify({
      // important
      startDate: moment(date).set({
        hour: 0,
        minute: 0,
        second: 0
      }),
      endDate: moment(date).set({
        hour: 23,
        minute: 59,
        second: 59
      })
    });
    // let startDate, endDate;
    // startDate = moment(date[0]);
    // endDate = moment(date[1]);
    // console.log(startDate);
    // console.log(endDate);
    // this.getInvoices('X-REPORTS', date[0], date[1]);x
    this.setState({ isLoaded: false });
    var hdr = localStorage.token;
    console.log(data);
    fetch(`${endpoint}/report`, {
      method: 'POST',
      body: data,
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            // // Magic, Do Not Touch
            // let data = result.data.map(item => ({
            //   ...item,
            //   createdDate: new Date(item.created).toLocaleDateString(),
            //   createdTime: new Date(item.created).toLocaleTimeString(),
            //   taxPrice: 'Kr.' + item.taxPrice.toFixed(2)
            // }));
            // var invoiceToLoad = data.reverse();

            let unitPriceTotal = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.productPrice;
              },
              0
            );

            // let finalTotal = result.data.reportData.reduce(
            //   (acc, current, index) => {
            //     return acc + current.productPrice * current.quantity;
            //   },
            //   0
            // );

            // let total15Quantity = result.data.reportData.reduce(
            //   (acc, current, index) => {
            //     return acc + current.unitoftax15;
            //   },
            //   0
            // );

            // let total25Quantity = result.data.reportData.reduce(
            //   (acc, current, index) => {
            //     return acc + current.unitoftax25;
            //   },
            //   0
            // );

            // let total25Tax = result.data.reportData.reduce(
            //   (acc, current, index) => {
            //     return (
            //       acc +
            //       (current.productPrice - current.productPrice / 1.25) *
            //         current.unitoftax25
            //     );
            //   },
            //   0
            // );

            // let total15Tax = result.data.reportData.reduce(
            //   (acc, current, index) => {
            //     return (
            //       acc +
            //       (current.productPrice - current.productPrice / 1.15) *
            //         current.unitoftax15
            //     );
            //   },
            //   0
            // );

            let finalTotal = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.total;
              },
              0
            );

            let total15Quantity = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.unitoftax15;
              },
              0
            );

            let total25Quantity = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.unitoftax25;
              },
              0
            );

            let total25Tax = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.taxPrice25;
              },
              0
            );

            let total15Tax = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.taxPrice15;
              },
              0
            );

            let totalDiscount = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.discountPrice;
              },
              0
            );

            this.setState({
              isLoaded: true,
              sendProps: true,
              items: result.data.reportData,
              tax: result.data.tax,
              // unitPriceTotal,
              branchInfo: result.data.branch,
              totalDiscount,
              finalTotal,
              total15Quantity,
              total25Quantity,
              total25Tax,
              total15Tax,
              unitPriceTotal
            });
            console.log(result);
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  goBackButton = () => {
    this.setState({ currentPage: 'buttons' });
  };

  render() {
    console.log(this.state);
    const { currentPage, isLoaded } = this.state;

    // const xColumns = [
    //   {
    //     title: 'Sr. No.',
    //     editable: false,
    //     width: '8%',
    //     render: (text, record, index) =>
    //       //? for correct sr no., check antd pagination and table/pagination docs
    //       //? index + (currentpage - 1)*10
    //       {
    //         return index + (this.state.paginationNo - 1) * 10 + 1;
    //         // return index + 1;
    //       }
    //   },
    //   {
    //     title: 'Name',
    //     dataIndex: 'productName',
    //     key: 'productName',
    //     width: '17%'
    //   },
    //   {
    //     title: 'Tax',
    //     width: '14%',
    //     children: [
    //       {
    //         title: '15%',
    //         width: '14%',
    //         render: (text, record) => {
    //           return `Kr.${(
    //             (record.productPrice - record.productPrice / 1.15) *
    //             record.unitoftax15
    //           ).toFixed(2)}`;
    //         }
    //       },
    //       {
    //         title: '25%',
    //         width: '14%',
    //         render: (text, record) => {
    //           return `Kr.${(
    //             (record.productPrice - record.productPrice / 1.25) *
    //             record.unitoftax25
    //           ).toFixed(2)}`;
    //         }
    //       }
    //     ]
    //   },
    //   {
    //     title: 'Unit Price',
    //     dataIndex: 'productPrice',
    //     key: 'productprice',
    //     width: '14%',
    //     render: text => {
    //       return `Kr.${text ? text.toFixed(2) : ''}`;
    //     }
    //   },
    //   {
    //     title: 'Quantity',
    //     width: '12%',
    //     children: [
    //       {
    //         title: '15%',
    //         dataIndex: 'unitoftax15',
    //         key: 'unitoftax15'
    //       },
    //       {
    //         title: '25%',
    //         dataIndex: 'unitoftax25',
    //         key: 'unitoftax15'
    //       }
    //     ]
    //   },
    //   {
    //     title: 'Total',
    //     key: 'total',
    //     dataIndex: 'total',
    //     width: '16%',
    //     render: (text, record) => {
    //       return `Kr.${(record.quantity * record.productPrice).toFixed(2)}`;
    //     }
    //   }
    // ];

    const xColumns = [
      {
        title: 'Sr. No.',
        editable: false,
        width: '8%',
        render: (text, record, index) =>
          //? for correct sr no., check antd pagination and table/pagination docs
          //? index + (currentpage - 1)*10
          {
            return index + (this.state.paginationNo - 1) * 10 + 1;
            // return index + 1;
          }
      },
      {
        title: 'Name',
        dataIndex: 'productName',
        key: 'productName',
        width: '15%'
      },
      // {
      //   title: '15%',
      //   width: '14%',
      //   render: (text, record) => {
      //     return `Kr.${(
      //       (record.productPrice - record.productPrice / 1.15) *
      //       record.unitoftax15
      //     ).toFixed(2)}`;
      //   }
      // },
      // {
      //   title: '25%',
      //   width: '14%',
      //   render: (text, record) => {
      //     return `Kr.${(
      //       (record.productPrice - record.productPrice / 1.25) *
      //       record.unitoftax25
      //     ).toFixed(2)}`;
      //   }
      // },
      {
        title: 'Tax',
        width: '14%',
        children: [
          {
            title: '15%',
            dataIndex: 'taxPrice15',
            render: text => {
              return `Kr.${text.toFixed(2)}`;
            }
          },
          {
            title: '25%',
            dataIndex: 'taxPrice25',
            render: text => {
              return `Kr.${text.toFixed(2)}`;
            }
          }
        ]
      },
      {
        title: 'Unit Price',
        dataIndex: 'productPrice',
        key: 'productprice',
        width: '14%',
        render: text => {
          return `Kr.${text ? text.toFixed(2) : ''}`;
        }
      },
      {
        title: 'Quantity',
        width: '11%',
        children: [
          {
            title: '15%',
            dataIndex: 'unitoftax15',
            key: 'unitoftax15'
          },
          {
            title: '25%',
            dataIndex: 'unitoftax25',
            key: 'unitoftax25'
          }
        ]
      },
      {
        title: 'Discount',
        key: 'discountPrice',
        dataIndex: 'discountPrice',
        width: '13%',
        render: text => {
          return `Kr.${text.toFixed(2)}`;
        }
      },
      {
        title: 'Total',
        key: 'total',
        dataIndex: 'total',
        width: '14%',
        render: text => {
          return `Kr.${text ? text.toFixed(2) : ''}`;
        }
      }
    ];

    // const summaryColumns = [
    //   {
    //     title: 'Sr. No.',
    //     width: '8%'
    //   },
    //   {
    //     title: 'Name',
    //     width: '17%',
    //     render: () => <strong>Total</strong>
    //   },
    //   {
    //     title: 'Total 15% Tax',
    //     width: '14%',
    //     render: () => `Kr.${this.state.total15Tax.toFixed(2)}`
    //   },
    //   {
    //     title: 'Total 25% Tax',
    //     width: '14%',
    //     render: () => `Kr.${this.state.total25Tax.toFixed(2)}`
    //   },
    //   {
    //     title: 'Total Unit Price',
    //     width: '14%',
    //     render: () => `Kr.${this.state.unitPriceTotal.toFixed(2)}`
    //   },
    //   {
    //     title: 'Total Quantity',
    //     width: '12%',
    //     children: [
    //       {
    //         title: '15%',
    //         dataIndex: 'unitoftax15',
    //         key: 'unitoftax15',
    //         render: () => this.state.total15Quantity
    //       },
    //       {
    //         title: '25%',
    //         dataIndex: 'unitoftax25',
    //         key: 'unitoftax15',
    //         render: () => this.state.total25Quantity
    //       }
    //     ]
    //   },
    //   {
    //     title: 'Final Total',
    //     width: '16%',
    //     render: () => `Kr.${this.state.finalTotal.toFixed(2)}`
    //   }
    // ];

    // const summaryData = [
    //   {
    //     key: '-1',
    //     // productName: 'Total',
    //     // productPrice: this.state.unitPriceTotal,
    //     // // total: this.state.finalTotal,
    //     // total: 'total',
    //     // Total: 'total',
    //     // unitoftax15: this.state.total15Quantity,
    //     // unitoftax25: this.state.total25Quantity
    //     render: () => 'test'
    //   }
    // ];

    const summaryColumns = [
      {
        title: 'Sr. No.',
        width: '8%'
      },
      {
        title: 'Name',
        width: '15%',
        render: () => <strong>Total</strong>
      },

      {
        title: 'Tax',
        width: '12%',
        children: [
          {
            title: 'Total 15% Tax',
            render: () => `Kr.${this.state.total15Tax.toFixed(2)}`
          },
          {
            title: 'Total 25% Tax',
            render: () => `Kr.${this.state.total25Tax.toFixed(2)}`
          }
        ]
      },

      {
        title: 'Total Unit Price',
        width: '14%',
        render: () => `Kr.${this.state.unitPriceTotal.toFixed(2)}`
      },
      {
        title: 'Total Quantity',
        width: '11%',
        children: [
          {
            title: '15%',
            dataIndex: 'unitoftax15',
            key: 'unitoftax15',
            render: () => this.state.total15Quantity
          },
          {
            title: '25%',
            dataIndex: 'unitoftax25',
            key: 'unitoftax25',
            render: () => this.state.total25Quantity
          }
        ]
      },
      {
        title: 'Discount',
        width: '13%',
        render: () => `Kr.${this.state.totalDiscount.toFixed(2)}`
      },
      {
        title: 'Final Total',
        width: '14%',
        render: () => `Kr.${this.state.finalTotal.toFixed(2)}`
      }
    ];

    const summaryData = [
      {
        key: '-1',
        // productName: 'Total',
        // productPrice: this.state.unitPriceTotal,
        // // total: this.state.finalTotal,
        // total: 'total',
        // Total: 'total',
        // unitoftax15: this.state.total15Quantity,
        // unitoftax25: this.state.total25Quantity
        render: () => 'test'
      }
    ];

    if (currentPage === 'buttons') {
      return (
        <Home isAdmin={true}>
          <div className="report-container-flex">
            <button className="box shadow stock-bg">
              <p className="text-items">TACCOUNTANT</p>
            </button>
          </div>
          <div className="different-reports-t">
            <div id="flex-stock">
              <button className="box-t shadow orange-bg">
                <p className="text-items">ALL INVOICE</p>
              </button>
              <button className="box-t shadow orange-bg">
                <p className="text-items">NOTIFICATION</p>
              </button>
              <button className="box-t shadow orange-bg">
                <p className="text-items">PROGRESS CHART</p>
              </button>
              <button className="box-t shadow orange-bg">
                <p className="text-items">HRM RAP</p>
              </button>
              <button className="box-t shadow orange-bg">
                <p className="text-items">IK SYSTEM RAP</p>
              </button>
            </div>
            <div id="flex-stock">
              <button className="box-t shadow black-bg">
                <p className="text-items">STOCK RAP</p>
              </button>
              <button className="box-t shadow black-bg">
                <p className="text-items">ADD FILES</p>
              </button>
              <button className="box-t shadow black-bg">
                <p className="text-items">CASH REP.</p>
              </button>
              <button className="box-t shadow black-bg">
                <p className="text-items" />
              </button>
              <button className="box-t shadow black-bg">
                <p className="text-items" />
              </button>
            </div>
            <div id="flex-stock">
              <button className="box-t shadow orange-bg">
                <p className="text-items">CUSTOMIZE</p>
              </button>
              <button className="box-t shadow orange-bg">
                <p className="text-items">BANK RAP</p>
              </button>
              <button className="box-t shadow orange-bg">
                <p className="text-items">WEB RAP</p>
              </button>
              <button className="box-t shadow orange-bg">
                <p className="text-items">IFL RAP</p>
              </button>
              <button
                className="box-t shadow orange-bg"
                onClick={() => this.handlePageChange('POS-RAP')}
              >
                <p className="text-items">POS RAP</p>
              </button>
            </div>
          </div>
        </Home>
      );
    } else if (currentPage === 'POS-RAP') {
      return (
        <Home>
          <div className="report-container">
            <span className="item">POS RAP</span>
            <span className="item">
              <DatePicker
                onChange={(date, dateString) =>
                  this.loadReports(date, dateString)
                }
                format="YYYY-MM-DD"
                placeholder="Select day"
              />
            </span>
            <span className="item">
              <ReactToPrint
                trigger={() => (
                  <button className="back-button-border">Print</button>
                )}
                content={() => this.componentRef}
                copyStyles={true}
              />
            </span>
            <span className="item">
              <span id="less-visible">
                <div onClick={this.goBackButton} className="back-button-border">
                  <i className="fa fa-arrow-circle-left" aria-hidden="true" />
                  <span>Back</span>
                </div>
              </span>
            </span>
          </div>
          <TableSum
            loading={isLoaded ? false : true}
            columns={xColumns}
            summaryColumns={summaryColumns}
            data={this.state.items}
            summaryData={summaryData}
            onChange={pagination => this.pageChange(pagination)}
            bordered
          />
          <div style={{ display: 'none' }}>
            {this.state.sendProps ? (
              <PrintReport
                columns={xColumns}
                summaryColumns={summaryColumns}
                data={this.state.items}
                summaryData={summaryData}
                ref={el => (this.componentRef = el)}
                branch={this.state.branchInfo}
                total15Tax={this.state.total15Tax}
                total25Tax={this.state.total25Tax}
                total15Quantity={this.state.total15Quantity}
                total25Quantity={this.state.total25Quantity}
                totalDiscount={this.state.totalDiscount}
                finalTotal={this.state.finalTotal}
                type="x-reports"
              />
            ) : (
              ''
            )}
          </div>
        </Home>
      );
    }
  }
}
const mapStateToProps = state => {
  return {};
};
const taccountant = connect(mapStateToProps)(taccountantComponent);
export default taccountant;
