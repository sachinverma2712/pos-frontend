import React, { Component, Fragment } from 'react';
import Home from '../Home/Home';
import ReactToPrint from 'react-to-print';
import './Reports.css';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import ReportsButton from '../ReportsButton/ReportsButton';
import { DatePicker, Input, Form, Button, notification, Select } from 'antd';
import PrintReport from '../PrintReport/PrintReport';
import endpoint from '../../helpers/endpoint';
import SingleInvoice from '../SingleInvoice/SingleInvoice';
import SingleOrder from '../SingleOrder/SingleOrder';
import TableSum from '../TableSum/TableSum';
import moment from 'moment';
import { subscribeToOrder } from '../../helpers/socket';

const { MonthPicker, WeekPicker } = DatePicker;
const Option = Select.Option;

class ReportsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      sendProps: false,
      error: false,
      clicked: false,
      items: [],
      branchInfo: '',
      title: 'Reports',
      startDate: '',
      endDate: '',
      year: '',
      currentPage: 'buttons',
      paginationNo: 1,
      billData: '',
      orderData: '',
      tax: '',
      unitPriceTotal: 0,
      total15Tax: 0,
      total25Tax: 0,
      total15Quantity: 0,
      total25Quantity: 0,
      finalTotal: 0,
      totalSubtotal: 0,
      totalTax: 0,
      totalTaxPrice: 0,
      totalDiscount: 0,
      orders: 0
    };

    subscribeToOrder((err, orders) =>
      this.setState({
        orders
      })
    );
  }

  componentDidMount() {
    let hdr = localStorage.token;
    fetch(`${endpoint}/ordercount`, {
      method: 'GET',
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(res => this.setState({ orders: res.data }))
      .catch(err => console.log(err));
  }

  getInvoices(title, startDate, endDate) {
    // let startDate;
    // let endDate;
    // if (title === 'DAILY REPORTS') {
    //   startDate = moment()
    //     .subtract(1, 'days')
    //     .startOf('day')
    //     .toString();
    //   endDate = moment()
    //     .subtract(1, 'days')
    //     .endOf('day')
    //     .toString();
    // }
    this.setState({ isLoaded: false });
    let data;
    if (title !== 'X-REPORTS') {
      data = JSON.stringify({
        startDate: moment(startDate).set({
          hour: 0,
          minute: 0,
          second: 0
        }),
        endDate: moment(endDate).set({
          hour: 23,
          minute: 59,
          second: 59
        })
      });
    } else {
      data = JSON.stringify({
        startDate: moment(startDate),
        endDate: moment(endDate)
      });
    }
    console.log(data);
    let hdr = localStorage.token;
    fetch(`${endpoint}/invoice`, {
      method: 'POST',
      body: data,
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            console.log(result.data);
            this.openNotification('error', result.data);
          } else {
            //! Magic, Do Not Touch
            console.log(result);
            let branchInfo = result.data.branch;
            let data = result.data.invoiceData.map(item => ({
              ...item,
              createdDate: new Date(item.created).toLocaleDateString(),
              createdTime: new Date(item.created).toLocaleTimeString(),
              taxPrice: item.taxPrice.toFixed(2),
              isLoaded: true
            }));
            let invoiceToLoad = data.reverse();
            this.setState({
              isLoaded: true,
              items: invoiceToLoad,
              branchInfo,
              sendProps: true
            });

            if (result.data.invoiceData.length > 0) {
              let totalSubtotal = result.data.invoiceData.reduce(
                (acc, current, index) => {
                  return (
                    acc +
                    current.taxPrice -
                    (current.taxPrice -
                      (100 * current.taxPrice) / (100 + current.tax))
                  );
                },
                0
              );

              let totalTax = result.data.invoiceData.reduce(
                (acc, current, index) => {
                  return (
                    acc +
                    (current.taxPrice -
                      (100 * current.taxPrice) / (100 + current.tax))
                  );
                },
                0
              );

              let totalTaxPrice = result.data.invoiceData.reduce(
                (acc, current, index) => {
                  return acc + current.taxPrice;
                },
                0
              );

              this.setState({ totalSubtotal, totalTax, totalTaxPrice });
            }
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  // handleClick = title => {
  //   this.setState(prevState => {
  //     return { clicked: !prevState.clicked, title: title };
  //   }, this.getInvoices(title, moment().startOf('day')._d, moment().endOf('day')._d));
  // };

  //On page load
  handleClick = title => {
    // this.setState(prevState => {
    //   return { clicked: !prevState.clicked, title: title };
    // }, this.sendDate(title));

    if (
      title === 'BANK REPORTS' ||
      title === 'TFL REPORTS' ||
      title === 'VIPPS REPORTS'
    ) {
      this.setState({ currentPage: 'buttons' });
    } else {
      this.setState(
        {
          items: '',
          currentPage: 'reports',
          title
        },
        this.sendDate(title)
      );
    }
  };

  sendDate = title => {
    let startDate, endDate, date;
    switch (title) {
      case 'DAILY REPORTS':
        startDate = moment().date()._d;
        endDate = moment().date()._d;
        return this.getInvoices(title, startDate, endDate);
      case 'WEEKLY REPORTS':
        // startDate = moment().startOf('week')._d;
        // endDate = moment().endOf('week')._d;
        startDate = moment().startOf('week');
        endDate = moment().endOf('week');
        return this.getInvoices(title, startDate, endDate);
      case 'MONTHLY REPORTS':
        startDate = moment().startOf('month');
        endDate = moment().endOf('month');
        return this.getInvoices(title, startDate, endDate);
      case 'YEARLY REPORTS':
        startDate = moment().startOf('year');
        endDate = moment().endOf('year');
        return this.getInvoices(title, startDate, endDate);
      case 'X-REPORTS':
        date = moment().date()._d;
        return this.onOk(date);
      case 'ONLINE ORDERS':
        return this.getOnlineOrders();
      default:
        return 0;
    }
  };

  getOnlineOrders = filter => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/getorders`, {
      method: 'GET',
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === 'failure') {
          // this.openNotification('error', result.data);
        } else {
          console.log('online', result);

          let items = [];
          let onlineTotal = '';

          switch (filter) {
            case 'all':
              items = result.data;
              onlineTotal = items.reduce(
                (acc, current) =>
                  acc +
                  current.items.reduce((acc, curr) => acc + curr.totalPrice, 0),
                0
              );
              break;
            case 'active':
              items = result.data.filter(
                item => item.orderStatus === 'IN-PROCESS'
              );
              onlineTotal = items.reduce(
                (acc, current) =>
                  acc +
                  current.items.reduce((acc, curr) => acc + curr.totalPrice, 0),
                0
              );
              break;
            case 'delivered':
              items = result.data.filter(
                item => item.orderStatus === 'DELIVERED'
              );

              onlineTotal = items.reduce(
                (acc, current) =>
                  acc +
                  current.items.reduce((acc, curr) => acc + curr.totalPrice, 0),
                0
              );
              break;
            case 'cancelled':
              items = result.data.filter(item => item.orderStatus === 'CANCEL');
              onlineTotal = items.reduce(
                (acc, current) =>
                  acc +
                  current.items.reduce((acc, curr) => acc + curr.totalPrice, 0),
                0
              );
              break;
            default:
              items = result.data;
              onlineTotal = items.reduce(
                (acc, current) =>
                  acc +
                  current.items.reduce((acc, curr) => acc + curr.totalPrice, 0),
                0
              );
              break;
          }

          this.setState({
            items,
            onlineTotal,
            isLoaded: true,
            sendProps: true
          });
        }
      })
      .catch(error => {
        this.setState({
          isLoaded: true,
          error
        });
      });
  };

  changeStatus = (type, id) => {
    const data = JSON.stringify({ orderStatus: type, id });
    console.log('api', data);
    let hdr = localStorage.token;
    fetch(`${endpoint}/onlineinvoicecreation`, {
      method: 'POST',
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      },
      body: data
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === 'failure') {
          // this.openNotification('error', result.data);
        } else {
          console.log('status', result);
          this.setState(
            {
              currentPage: 'reports'
            },
            () => this.getOnlineOrders()
          );
        }
      })
      .catch(error => {
        this.setState({
          error
        });
      });
  };
  // when someone changes the date
  onChange = (title, date, dateString) => {
    // console.log(dateString);
    // let gg = moment()
    //   .day('Monday')
    //   .week(40);
    // console.log(gg);
    // console.log(gg.add(7, 'days'));

    let startDate, endDate;
    switch (title) {
      case 'DAILY REPORTS':
        startDate = date;
        endDate = date;
        return this.getInvoices(title, startDate, endDate);
      case 'WEEKLY REPORTS':
        startDate = moment(date).startOf('week');
        endDate = moment(date).endOf('week');
        return this.getInvoices(title, startDate, endDate);
      case 'MONTHLY REPORTS':
        startDate = moment(date).startOf('month');
        endDate = moment(date).endOf('month');
        return this.getInvoices(title, startDate, endDate);
      case 'YEARLY REPORTS':
        startDate = moment(date).startOf('year');
        endDate = moment(date).endOf('year');
        return this.getInvoices(title, startDate, endDate);
      default:
        return 'error';
    }
  };

  pageChange = pagination => {
    if (pagination) {
      this.setState({ paginationNo: pagination.current });
    }
  };

  onOk = date => {
    this.setState({ today: date });
    let data = JSON.stringify({
      // important
      startDate: moment(date).set({
        hour: 0,
        minute: 0,
        second: 0
      }),
      endDate: moment(date).set({
        hour: 23,
        minute: 59,
        second: 59
      })
    });
    // let startDate, endDate;
    // startDate = moment(date[0]);
    // endDate = moment(date[1]);
    // console.log(startDate);
    // console.log(endDate);
    // this.getInvoices('X-REPORTS', date[0], date[1]);x
    this.setState({ isLoaded: false });
    let hdr = localStorage.token;
    console.log(data);
    fetch(`${endpoint}/report`, {
      method: 'POST',
      body: data,
      headers: {
        'x-auth-token': hdr,
        'Content-Type': 'application/json'
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.data);
          } else {
            console.log(result);
            // // Magic, Do Not Touch
            // let data = result.data.map(item => ({
            //   ...item,
            //   createdDate: new Date(item.created).toLocaleDateString(),
            //   createdTime: new Date(item.created).toLocaleTimeString(),
            //   taxPrice: 'Kr.' + item.taxPrice.toFixed(2)
            // }));
            // let invoiceToLoad = data.reverse();
            let unitPriceTotal = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.productPrice;
              },
              0
            );

            let finalTotal = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.total;
              },
              0
            );

            let total15Quantity = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.unitoftax15;
              },
              0
            );

            let total25Quantity = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.unitoftax25;
              },
              0
            );

            let total25Tax = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.taxPrice25;
              },
              0
            );

            let total15Tax = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.taxPrice15;
              },
              0
            );

            let totalDiscount = result.data.reportData.reduce(
              (acc, current, index) => {
                return acc + current.discountPrice;
              },
              0
            );

            this.setState({
              isLoaded: true,
              sendProps: true,
              items: result.data.reportData,
              tax: result.data.tax,
              branchInfo: result.data.branch,
              finalTotal,
              total15Quantity,
              total25Quantity,
              total25Tax,
              total15Tax,
              totalDiscount,
              unitPriceTotal
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  // onOk = (date, dateString) => {
  //   console.log(date);
  //   console.log(dateString);
  //   // let startDate, endDate;
  //   // startDate = moment(date[0]);
  //   // endDate = moment(date[1]);
  //   // console.log(startDate);
  //   // console.log(endDate);
  //   // this.getInvoices('X-REPORTS', date[0], date[1]);x
  //   this.setState({ isLoaded: false });
  //   let hdr = localStorage.token;
  //   let data = JSON.stringify({ startDate: date[0]._d, endDate: date[1]._d });
  //   console.log(data);
  //   fetch(`${endpoint}/report`, {
  //     method: 'POST',
  //     body: data,
  //     headers: {
  //       'x-auth-token': hdr,
  //       'Content-Type': 'application/json'
  //     }
  //   })
  //     .then(res => res.json())
  //     .then(
  //       result => {
  //         if (result.status === 'failure') {
  //           alert('errors');
  //         } else {
  //           console.log(result);
  //           // // Magic, Do Not Touch
  //           // let data = result.data.map(item => ({
  //           //   ...item,
  //           //   createdDate: new Date(item.created).toLocaleDateString(),
  //           //   createdTime: new Date(item.created).toLocaleTimeString(),
  //           //   taxPrice: 'Kr.' + item.taxPrice.toFixed(2)
  //           // }));
  //           // let invoiceToLoad = data.reverse();
  //           this.setState({
  //             isLoaded: true,
  //             items: result.data.reportData,
  //             tax: result.data.tax
  //           });
  //           console.log(result);
  //         }
  //       },
  //       // Note: it's important to handle errors here
  //       // instead of a catch() block so that we don't swallow
  //       // exceptions from actual bugs in components.
  //       error => {
  //         this.setState({
  //           isLoaded: true,
  //           error
  //         });
  //       }
  //     );
  // };

  goBackButton = () => {
    let { currentPage } = this.state;
    if (currentPage === 'reports') {
      this.setState({
        currentPage: 'buttons'
      });
    } else if (currentPage === 'invoice' || currentPage === 'order') {
      this.setState({
        currentPage: 'reports'
      });
    }
  };

  searchType = e => {
    this.setState({ year: e.target.value });
  };

  showInvoice = invoiceNumber => {
    let index = this.state.items.findIndex(
      invoice => invoice.invoiceNumber === invoiceNumber
    );
    this.setState({
      currentPage: 'invoice',
      billData: this.state.items[index]
    });
  };

  showOrder = id => {
    console.log('items', this.state.items);
    let index = this.state.items.findIndex(item => item._id === id);
    this.setState({
      currentPage: 'order',
      orderData: this.state.items[index]
    });
  };

  searchYear = e => {
    console.log(e.key);
    // e.preventDefault();
    let startDate = moment(e.key);
    console.log(startDate);
    let endDate = moment(startDate).endOf('year');
    return this.getInvoices('YEARLY REPORTS', startDate, endDate);
  };

  handleSwitch = title => {
    switch (title) {
      case 'DAILY REPORTS':
        return (
          <DatePicker
            onChange={(date, dateString) =>
              this.onChange(title, date, dateString)
            }
            format='YYYY-MM-DD'
            placeholder='Select day'
          />
        );
      case 'WEEKLY REPORTS':
        return (
          <WeekPicker
            onChange={(date, dateString) =>
              this.onChange(title, date, dateString)
            }
            placeholder='Select week'
          />
        );
      case 'MONTHLY REPORTS':
        return (
          <MonthPicker
            onChange={(date, dateString) =>
              this.onChange(title, date, dateString)
            }
            placeholder='Select month'
          />
        );
      case 'YEARLY REPORTS':
        const currentYear = new Date().getFullYear();
        const arr = [];
        for (let i = 2018; i <= currentYear; i++) {
          arr.push(i.toString());
        }
        return (
          <>
            <Select
              labelInValue
              defaultValue={{ key: '2019' }}
              style={{ width: 120 }}
              onChange={e => this.searchYear(e)}
            >
              {arr.map(year => (
                <Option value={year}>{year}</Option>
              ))}
            </Select>
          </>
        );
      case 'X-REPORTS':
        return (
          /* <DatePicker
            onChange={(date, dateString) => this.onOk(date, dateString)}
            format="YYYY-MM-DD"
            placeholder="Select day"
          /> */
          <span>Today's Reports</span>
        );
      // case 'X-REPORTS':
      //   return (
      //     <RangePicker
      //       showTime={{ format: 'HH:mm' }}
      //       format="YYYY-MM-DD HH:mm"
      //       placeholder={['Start Time', 'End Time']}
      //       onOk={date => this.onOk(date)}
      //     />
      //   );
      default:
        return '';
    }
  };

  render() {
    console.log(this.state);
    const firstButtonItems = [
      'DAILY REPORTS',
      'WEEKLY REPORTS',
      'MONTHLY REPORTS',
      'YEARLY REPORTS'
    ];

    const secondButtonItems = [
      'X-REPORTS',
      // 'BANK REPORTS',
      'ONLINE ORDERS',
      'VIPPS REPORTS',
      'TFL REPORTS'
    ];

    const ordersSummaryColumn = [
      {
        title: 'Total',
        width: '72%',
        render: () => <strong>Total</strong>
      },
      {
        title: 'Total',
        render: () => `Kr.${this.state.onlineTotal}`
      }
    ];

    const dailySummaryColumns = [
      {
        title: 'emp',
        width: '9%',
        render: () => ``
      },
      {
        title: 'Total',
        width: '14%',
        render: () => <strong>Total</strong>
      },
      {
        title: 'emp',
        width: '10%'
      },
      {
        title: 'emp',
        width: '10%'
      },
      {
        title: 'Sub-Total',
        width: '13%',
        render: () => `Kr.${this.state.totalSubtotal.toFixed(2)}`
      },
      {
        title: 'Tax',
        width: '13%',
        render: () => `Kr.${this.state.totalTax.toFixed(2)}`
      },
      {
        title: 'Total',
        width: '13%',
        render: () => `Kr.${this.state.totalTaxPrice.toFixed(2)}`
      },
      {
        title: 'emp',
        width: '12%'
      }
    ];

    const summaryColumns = [
      {
        title: 'Sr. No.',
        width: '8%'
      },
      {
        title: 'Name',
        width: '15%',
        render: () => <strong>Total</strong>
      },

      {
        title: 'Tax',
        width: '12%',
        children: [
          {
            title: 'Total 15% Tax',
            render: () => `Kr.${this.state.total15Tax.toFixed(2)}`
          },
          {
            title: 'Total 25% Tax',
            render: () => `Kr.${this.state.total25Tax.toFixed(2)}`
          }
        ]
      },

      {
        title: 'Total Unit Price',
        width: '14%',
        render: () => `Kr.${this.state.unitPriceTotal.toFixed(2)}`
      },
      {
        title: 'Total Quantity',
        width: '11%',
        children: [
          {
            title: '15%',
            dataIndex: 'unitoftax15',
            key: 'unitoftax15',
            render: () => this.state.total15Quantity
          },
          {
            title: '25%',
            dataIndex: 'unitoftax25',
            key: 'unitoftax25',
            render: () => this.state.total25Quantity
          }
        ]
      },
      {
        title: 'Discount',
        width: '13%',
        render: () => `Kr.${this.state.totalDiscount.toFixed(2)}`
      },
      {
        title: 'Final Total',
        width: '14%',
        render: () => `Kr.${this.state.finalTotal.toFixed(2)}`
      }
    ];

    const summaryData = [
      {
        key: '-1',
        // productName: 'Total',
        // productPrice: this.state.unitPriceTotal,
        // // total: this.state.finalTotal,
        // total: 'total',
        // Total: 'total',
        // unitoftax15: this.state.total15Quantity,
        // unitoftax25: this.state.total25Quantity
        render: () => 'test'
      }
    ];

    const xColumns = [
      {
        title: 'Sr. No.',
        editable: false,
        width: '8%',
        render: (text, record, index) =>
          //? for correct sr no., check antd pagination and table/pagination docs
          //? index + (currentpage - 1)*10
          {
            return index + (this.state.paginationNo - 1) * 10 + 1;
            // return index + 1;
          }
      },
      {
        title: 'Name',
        dataIndex: 'productName',
        key: 'productName',
        width: '15%'
      },
      // {
      //   title: '15%',
      //   width: '14%',
      //   render: (text, record) => {
      //     return `Kr.${(
      //       (record.productPrice - record.productPrice / 1.15) *
      //       record.unitoftax15
      //     ).toFixed(2)}`;
      //   }
      // },
      // {
      //   title: '25%',
      //   width: '14%',
      //   render: (text, record) => {
      //     return `Kr.${(
      //       (record.productPrice - record.productPrice / 1.25) *
      //       record.unitoftax25
      //     ).toFixed(2)}`;
      //   }
      // },
      {
        title: 'Tax',
        width: '14%',
        children: [
          {
            title: '15%',
            dataIndex: 'taxPrice15',
            render: text => {
              return text ? `Kr.${text.toFixed(2)}` : text;
            }
          },
          {
            title: '25%',
            dataIndex: 'taxPrice25',
            render: text => {
              return `Kr.${text.toFixed(2)}`;
            }
          }
        ]
      },
      {
        title: 'Unit Price',
        dataIndex: 'productPrice',
        key: 'productPrice',
        width: '14%',
        render: text => {
          return `Kr.${text ? text.toFixed(2) : ''}`;
        }
      },
      {
        title: 'Quantity',
        width: '11%',
        children: [
          {
            title: '15%',
            dataIndex: 'unitoftax15',
            key: 'unitoftax15'
          },
          {
            title: '25%',
            dataIndex: 'unitoftax25',
            key: 'unitoftax25'
          }
        ]
      },
      {
        title: 'Discount',
        key: 'discountPrice',
        dataIndex: 'discountPrice',
        width: '13%',
        render: text => {
          return `Kr.${text.toFixed(2)}`;
        }
      },
      {
        title: 'Total',
        key: 'total',
        dataIndex: 'total',
        width: '14%',
        render: text => {
          return `Kr.${text ? text.toFixed(2) : ''}`;
        }
      }
    ];

    const ordersColumns = [
      {
        title: 'Sr. No.',
        key: 'orderNumber',
        width: '5%',
        render: (text, record, index) => index + 1
      },
      {
        title: 'Order time',
        key: 'orderTime',
        width: '18%',
        render: (text, record) =>
          `${new Date(record.createdAt).toLocaleDateString()} - ${new Date(
            record.createdAt
          ).toLocaleTimeString()}`
      },
      {
        title: 'No. of products',
        key: 'noProducts',
        width: '9%',
        render: (text, record) => record.items.length
      },

      {
        title: 'Status',
        key: 'status',
        width: '12%',
        render: (text, record) => record.orderStatus
      },
      {
        title: 'Order Type',
        key: 'status',
        width: '9%',
        render: (text, record) => record.orderType
      },
      {
        title: 'Total',
        key: 'total',
        width: '9%',
        render: (text, record) =>
          `Kr.${record.items.reduce(
            (acc, current) => acc + current.totalPrice,
            0
          )}`
      },
      {
        title: 'View',
        key: 'view',
        width: '10%',
        render: (text, record) => (
          <Button onClick={() => this.showOrder(record._id)}>View</Button>
        )
      }
    ];

    const dailyColumns = [
      {
        title: 'Invoice No.',
        dataIndex: 'invoiceNumber',
        key: 'invoiceNumber',
        width: '9%'
      },
      {
        title: 'Date',
        dataIndex: 'createdDate',
        key: 'createdDate',
        width: '15%'
      },
      {
        title: 'Time',
        dataIndex: 'createdTime',
        key: 'createdTime',
        width: '12%'
      },
      {
        title: 'Order Type',
        dataIndex: 'orderType',
        key: 'orderType',
        width: '10%'
      },
      {
        title: 'Sub-Total',
        width: '15%',
        render: (text, record) => {
          return `Kr.${(
            record.taxPrice -
            (
              record.taxPrice -
              (100 * record.taxPrice) / (100 + record.tax)
            ).toFixed(2)
          ).toFixed(2)}`;
        }
      },
      {
        title: 'Tax',
        width: '15%',
        render: (text, record) => {
          return `Kr.${(
            record.taxPrice -
            (100 * record.taxPrice) / (100 + record.tax)
          ).toFixed(2)}`;
        }
      },
      {
        title: 'Total',
        dataIndex: 'taxPrice',
        key: 'taxPrice',
        width: '15%',
        render: (text, record) => {
          return `Kr.${text}`;
        }
      },
      {
        title: 'Invoice',
        key: '_id',
        width: '9%',
        render: (text, record) => (
          <Button onClick={() => this.showInvoice(record.invoiceNumber)}>
            View
          </Button>
        )
      }
    ];
    const { isLoaded, error, currentPage, title } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (currentPage === 'buttons') {
      return (
        <Home isAdmin={true}>
          <div className='report-container'>
            <span className='item'>REPORTS</span>
            <span className='item'>
              <span id='less-visible'>HOME / </span>
              REPORTS
            </span>
          </div>
          <div className='different-reports'>
            <div id='flex'>
              {firstButtonItems.map(x => (
                <ReportsButton
                  key={x}
                  title={x}
                  css='box shadow'
                  color='orange-bg'
                  clicked={() => this.handleClick(x)}
                />
              ))}
            </div>
            <div id='flex'>
              {secondButtonItems.map(x => (
                <ReportsButton
                  orders={this.state.orders}
                  key={x}
                  title={x}
                  css='box shadow'
                  color='black-bg'
                  clicked={() => this.handleClick(x)}
                />
              ))}
            </div>
          </div>
        </Home>
      );
    } else if (currentPage === 'reports') {
      return (
        <Home isAdmin={true}>
          <div className='report-container'>
            <span className='item'>
              <span id='less-visible'>HOME / </span>
              {this.state.title}
            </span>
            <span className='item'>{this.handleSwitch(this.state.title)}</span>
            {title === 'ONLINE ORDERS' && (
              <span className='item'>
                <button
                  className='back-button-border outlined  '
                  style={{ margin: '0 2px' }}
                  onClick={() => this.getOnlineOrders('all')}
                >
                  All
                </button>
                <button
                  className='back-button-border outlined'
                  style={{ margin: '0 2px' }}
                  onClick={() => this.getOnlineOrders('active')}
                >
                  Active
                </button>
                <button
                  className='back-button-border outlined'
                  style={{ margin: '0 2px' }}
                  onClick={() => this.getOnlineOrders('delivered')}
                >
                  Delivered
                </button>
                <button
                  className='back-button-border outlined'
                  style={{ margin: '0 2px' }}
                  onClick={() => this.getOnlineOrders('cancelled')}
                >
                  Cancelled
                </button>
              </span>
            )}
            {title !== 'ONLINE ORDERS' && (
              <span className='item'>
                <ReactToPrint
                  trigger={() => (
                    <button className='back-button-border'>Print</button>
                  )}
                  content={() => this.componentRef}
                  copyStyles={true}
                />
              </span>
            )}

            <span className='item'>
              <span id='less-visible'>
                <div onClick={this.goBackButton} className='back-button-border'>
                  <i className='fa fa-arrow-circle-left' aria-hidden='true' />
                  <span>Back</span>
                </div>
              </span>
            </span>
          </div>
          {title === 'X-REPORTS' && (
            <div>
              {/* <Table
                dataSource={this.state.items}
                columns={xColumns}
                rowkey="productName"
                onChange={pagination => this.pageChange(pagination)}
                footer={() => {
                  return (
                    <div style={{ textAlign: 'right', margin: '0 20px' }}>
                      <strong>Total: </strong>
                      {this.state.tax
                        ? `Kr.${this.state.tax.toFixed(2)}`
                        : this.state.tax}
                    </div>
                  );
                }}
              /> */}
              <TableSum
                loading={isLoaded ? false : true}
                columns={xColumns}
                summaryColumns={summaryColumns}
                data={this.state.items}
                summaryData={summaryData}
                onChange={pagination => this.pageChange(pagination)}
                rowKey={record => record.productName}
                bordered
              />
              <div style={{ display: 'none' }}>
                {this.state.sendProps ? (
                  <PrintReport
                    columns={xColumns}
                    summaryColumns={summaryColumns}
                    data={this.state.items}
                    summaryData={summaryData}
                    ref={el => (this.componentRef = el)}
                    branch={this.state.branchInfo}
                    total15Tax={this.state.total15Tax}
                    total25Tax={this.state.total25Tax}
                    total15Quantity={this.state.total15Quantity}
                    total25Quantity={this.state.total25Quantity}
                    totalDiscount={this.state.totalDiscount}
                    finalTotal={this.state.finalTotal}
                    unitPriceTotal={this.state.unitPriceTotal}
                    type='x-reports'
                  />
                ) : (
                  ''
                )}
              </div>
              {/* <div className="report-container">
                <span className="item">
                  <span id="less-visible">
                    Total Tax:{' '}
                    <b>
                      {this.state.tax
                        ? this.state.tax.toFixed(2)
                        : this.state.tax}
                    </b>
                  </span>
                </span>
              </div> */}
            </div>
          )}
          {(title === 'DAILY REPORTS' ||
            title === 'WEEKLY REPORTS' ||
            title === 'MONTHLY REPORTS' ||
            title === 'YEARLY REPORTS') && (
            <Fragment>
              {/* <Table
              loading={isLoaded ? false : true}
              dataSource={this.state.items}
              columns={dailyColumns}
              rowkey="name"
              onChange={pagination => this.pageChange(pagination)}
            /> */}
              <TableSum
                loading={isLoaded ? false : true}
                columns={dailyColumns}
                summaryColumns={dailySummaryColumns}
                data={this.state.items}
                summaryData={summaryData}
                onChange={pagination => this.pageChange(pagination)}
                rowKey={record => record.productName}
                bordered
              />
              <div style={{ display: 'none' }}>
                {this.state.sendProps ? (
                  <PrintReport
                    columns={dailyColumns}
                    summaryColumns={dailySummaryColumns}
                    data={this.state.items}
                    summaryData={summaryData}
                    ref={el => (this.componentRef = el)}
                    branch={this.state.branchInfo}
                    totalTaxPrice={this.state.totalTaxPrice}
                    totalSubtotal={this.state.totalSubtotal}
                    totalTax={this.state.totalTax}
                    type='daily-reports'
                  />
                ) : (
                  ''
                )}
              </div>
            </Fragment>
          )}
          {title === 'ONLINE ORDERS' && (
            <Fragment>
              <TableSum
                loading={isLoaded ? false : true}
                columns={ordersColumns}
                summaryColumns={ordersSummaryColumn}
                data={this.state.items}
                summaryData={summaryData}
                onChange={pagination => this.pageChange(pagination)}
                rowKey={record => record.productName}
                bordered
              />
              <div style={{ display: 'none' }}>
                {this.state.sendProps && (
                  <PrintReport
                    columns={ordersColumns}
                    summaryColumns={ordersSummaryColumn}
                    data={this.state.items}
                    summaryData={summaryData}
                    ref={el => (this.componentRef = el)}
                    branch={this.state.branchInfo}
                    totalTaxPrice={this.state.totalTaxPrice}
                    totalSubtotal={this.state.totalSubtotal}
                    totalTax={this.state.totalTax}
                    type='online'
                  />
                )}
              </div>
            </Fragment>
          )}
        </Home>
      );
    } else if (currentPage === 'invoice') {
      return (
        <Home isAdmin={true}>
          <div className='report-container'>
            <span className='item'>
              <span id='less-visible'>HOME / </span>
              Invoice No. {this.state.billData.invoiceNumber}
            </span>
            <span className='item'>
              <span id='less-visible'>
                <div onClick={this.goBackButton} className='back-button-border'>
                  <i className='fa fa-arrow-circle-left' aria-hidden='true' />
                  <span>Back</span>
                </div>
              </span>
            </span>
          </div>
          <SingleInvoice
            data={this.state.billData}
            branch={this.state.branchInfo}
          />
        </Home>
      );
    } else if (currentPage === 'order') {
      return (
        <>
          <Home isAdmin={true}>
            <div className='report-container'>
              <span className='item'>
                <span id='less-visible'>HOME / </span>
                Order Details
              </span>
              <span className='item'>
                <span id='less-visible'>
                  <div
                    onClick={this.goBackButton}
                    className='back-button-border'
                  >
                    <i className='fa fa-arrow-circle-left' aria-hidden='true' />
                    <span>Back</span>
                  </div>
                </span>
              </span>
            </div>
            <SingleOrder
              data={this.state.orderData}
              changeStatus={(type, id) => this.changeStatus(type, id)}
            />
          </Home>
        </>
      );
    } else {
      return <div />;
    }
  }
  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}

const mapStateToProps = state => {
  return {};
};
const Reports = connect(mapStateToProps)(ReportsComponent);
export default Reports;
