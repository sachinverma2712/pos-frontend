import React, { Component } from 'react';
import { Col, Button } from 'antd';

class IconGridItem extends Component {
  render() {
    return (
      <Col className="items gutter-row">
        <div className="button-style">
          <Button
            style={{ backgroundColor: this.props.color }}
            className="button-size-icon"
            id={this.props.id}
            onClick={() => this.props.clicked(this.props)}
          >
            <div className="menu-options white">
              <img src={this.props.icon} className="product-image" />
            </div>
            <span className="menu-options name">
              {this.props.category ? '' : `Kr. ${this.props.productPrice}`}
            </span>
            <div className="menu-options details">
              <span className="menu-options price">
                {this.props.productName}{' '}
              </span>
            </div>
          </Button>
        </div>
      </Col>
    );
  }
}

export default IconGridItem;
