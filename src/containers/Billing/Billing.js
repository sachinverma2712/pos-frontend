import React, { Component } from "react";
import Home from "../Home/Home";
import "./Billing.css";
import { connect } from "react-redux";
import { Col, Row, Input, notification } from "antd";
import Loader from "../Loader/Loader";
import Modal from "react-modal";
import FloatingButtons from "../../components/FloatingButtons/FloatingButtons";
import IconGridList from "../IconGridList/IconGridList";
import BillingForm from "../BillingForm/BillingForm";
import RightMenu from "../../components/RightMenu/RightMenu";
import endpoint from "../../helpers/endpoint";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)"
  }
};

class BillingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isBill: false,
      test: "abc",
      isMobile: true,
      items: [],
      categoryData: [],
      productData: [],
      filteredProducts: [],
      table: [],
      searchQuery: "",
      modalIsOpen: false,
      category: true,
      customer: "",
      tableEmail: "",
      tablePhoneNo: 0,
      type: "",
      discount: 0,
      cash: 0,
      driveoutCharge: 0,
      creditCheck: false,
      createdBy: "",
      createdOn: "",
      fields: {},
      errors: {}
    };
    this.toggleModal = this.toggleModal.bind(this);
  }

  handleOk = (type, discount) => {
    this.setState({ type, discount });
  };

  // ? Product Search, works fine, don't touch...
  searchProducts = searchQuery => {
    const { category, categoryData, productData } = this.state;
    let data = category ? categoryData : productData;
    let searchedProducts = data.filter(item => {
      searchQuery = searchQuery.toLowerCase();
      item.name = item.name.toLowerCase();
      return item.name.indexOf(searchQuery) > -1;
    });
    this.setState({
      searchQuery: searchQuery,
      filteredProducts: searchedProducts
    });
  };

  saveItems = () => {
    const { type, discount } = this.state;
    let hdr = localStorage.token;
    let table = localStorage.table;
    let data = JSON.stringify({ items: this.state.items, type, discount });
    fetch(`${endpoint}/table/${table}`, {
      method: "PUT",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification(
            "error",
            "Invoice already generated or something went wrong."
          );
        } else {
          this.openNotification(result.status, result.message);
          // this.props.history.push('/tableview');
        }
      });
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  findCustomer = e => {
    e.preventDefault();
    let hdr = localStorage.token;
    let table = localStorage.table;
    const { customer } = this.state;
    const regexNo = /^[0-9]+$/g;
    const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let type;
    console.log(customer);
    // if (customer.match(regexNo)) type = "phoneNo";
    // else
    if (customer.match(regexEmail)) type = "email";
    else
      return this.openNotification(
        "error",
        "Please enter a valid email or phone."
      );
    let data = JSON.stringify({
      customer,
      type
    });
    console.log(data);
    fetch(`${endpoint}/table/${table}`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              tableEmail: result.data.email,
              tablePhoneNo: result.data.phoneNo
            });
            this.toggleModal();
          }
        },
        error => {
          console.log(error);
          this.setState({
            error
          });
        }
      );
  };

  componentDidMount() {
    if (localStorage.table === null) {
      this.props.history.push("/tableview");
    }
    var hdr = localStorage.token;
    var tableId = localStorage.table;
    fetch(`${endpoint}/table/${tableId}`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            console.log(result);
            this.openNotification("error", result.data);
          } else {
            console.log("gggg", result.data);

            let type = "";

            if (
              result.data.orderType === "Take away" ||
              result.data.orderType === "Drive Out"
            ) {
              type = "PLU2";
            } else {
              type = "PLU1";
            }

            this.setState({
              isLoaded: true,
              table: result.data,
              items: result.data.items || [],
              orderType: result.data.orderType,
              tableEmail: result.data.customerId
                ? result.data.customerId.email
                : "",
              tablePhoneNo: result.data.customerId
                ? result.data.customerId.phoneNo
                : "",
              modalIsOpen: result.data.customerId ? false : true,
              createdOn: result.data.created,
              createdBy: result.data.employeeId,
              type,
              driveoutCharge: result.data.driveoutCharge
            });
          }
        },
        error => {
          this.setState({
            error
          });
        }
      );

    fetch(`${endpoint}/category`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            console.log("gg");
            this.setState({
              isLoaded: true,
              categoryData: result.data || []
            });
          }
        },
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  }

  cashGiven = cash => {
    this.setState({ cash: this.state.cash + cash });
  };

  toggleModal() {
    this.setState({ modalIsOpen: !this.state.modalIsOpen });
  }

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  addToBill = (e, productName) => {
    var any = Object.assign({}, this.state);
    any.productName = productName;
    this.setState(any, function() {});
    // console.log(e.target.value);
  };

  addProduct(productInfo) {
    var state = Object.assign({}, this.state);
    var product = Object.assign({}, productInfo);
    var index = state.items.findIndex(x => x.id === product.id);
    // console.log(index);
    if (index > -1) {
      state.items[index].quantity++;
    } else {
      product["quantity"] = 1;
      state.items.push(product);
    }
    this.setState(state);
  }

  handleClick = category => {
    if (category) {
      this.productPage(category.id);
    } else {
      console.log("beep bop");
    }
  };

  productPage = id => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/product/${id}`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              isLoaded: true,
              productData: result.data,
              category: false,
              catId: id
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  toggleform = e => {
    let state = Object.assign(this.state);
    state.form = !state.form;
    this.setState(state);
  };
  Closeform = e => {
    this.setState({ form: false });
  };
  static propTypes = {
    isBilling: true
  };

  // try not to touch
  deleted = id => {
    let index = this.state.items.findIndex(x => x.id === id);
    this.setState(this.state.items.splice(index, 1));
  };

  goToCat = () => {
    this.setState({ category: true });
  };

  handleQuantity = (e, id) => {
    const newItems = this.state.items.slice();
    newItems[id].quantity = e.target.value;
    this.setState({
      items: newItems
    });
  };

  creditCheck = () => {
    console.log("ggwp");
    this.setState({ creditCheck: true });
  };

  deleteTable = () => {
    var hdr = localStorage.token;
    let tableId = localStorage.table;
    fetch(`${endpoint}/table/${tableId}`, {
      method: "DELETE",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", "Could not delete table");
          } else {
            this.openNotification(result.status, result.message);
            this.props.history.push("/tables");
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  render() {
    console.log(this.state);
    const {
      error,
      isLoaded,
      cash,
      table,
      tableEmail,
      tablePhoneNo,
      type,
      discount
    } = this.state;
    if (!isLoaded) {
      return (
        <Home>
          <Loader />
        </Home>
      );
    } else if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <Home isBilling={true}>
          <Row gutter={20} className="billing">
            <Col span={9} className="bill-container">
              <div className="table-number">
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "space-between"
                  }}
                >
                  <span>
                    <span className="left-text small">Email: {tableEmail}</span>
                    <br />
                    <span className="left-text small">
                      Phone: {tablePhoneNo}
                    </span>
                  </span>
                  <span className="right-text">
                    {table.number} / Table
                    <i
                      style={{
                        padding: 5,
                        marginLeft: 10,
                        width: 30,
                        height: 30,
                        textAlign: "center",
                        lineHeight: "20px",
                        borderRadius: "50%",
                        background: "#444",
                        color: "#fefefe"
                      }}
                      className="fa fa-trash"
                      aria-hidden="true"
                      onClick={() => this.deleteTable()}
                    />
                  </span>
                </div>
              </div>
              <BillingForm
                orderType={this.state.orderType}
                loaded={this.state.isLoaded}
                clicked={this.saveItems}
                tax={table.tax}
                product={this.state.items}
                delete={id => this.deleted(id)}
                changeQuantity={(e, id) => this.handleQuantity(e, id)}
                type={type}
                discount={discount}
                cash={cash}
                creditCheck={this.state.creditCheck}
                createdBy={this.state.createdBy}
                createdOn={this.state.createdOn}
                driveoutCharge={this.state.driveoutCharge}
              />
            </Col>
            <Col span={13}>
              <div>
                <Row className="dashboard-flex billing">
                  <span className="ant-input-search order-search ant-input-search-enter-button ant-input-affix-wrapper">
                    <Input
                      placeholder="Search"
                      className="ant-input"
                      type="text"
                      value={this.state.searchQuery}
                      onChange={e => this.searchProducts(e.target.value)}
                    />
                    <span className="ant-input-suffix"> </span>
                  </span>
                  {!this.state.category ? (
                    <div onClick={this.goToCat} className="back-button">
                      <i class="fa fa-arrow-circle-left" aria-hidden="true" />
                      <span>Back</span>
                    </div>
                  ) : null}

                  {this.state.category ? (
                    <IconGridList
                      isBilling={true}
                      type={this.state.type}
                      products={
                        this.state.searchQuery === ""
                          ? this.state.categoryData
                          : this.state.filteredProducts
                      }
                      handleClick={categories => this.handleClick(categories)}
                      category={this.state.category}
                    />
                  ) : (
                    <IconGridList
                      isBilling={false}
                      type={this.state.type}
                      products={
                        this.state.searchQuery === ""
                          ? this.state.productData
                          : this.state.filteredProducts
                      }
                      handleClick={productInfo => this.addProduct(productInfo)}
                      category={this.state.category}
                    />
                  )}
                  {this.state.searchQuery &&
                  this.state.filteredProducts.length === 0 ? (
                    <div>No products found</div>
                  ) : (
                    ""
                  )}
                </Row>
              </div>
              <Col className="bottom-bar">
                <FloatingButtons dispatch={this.props.dispatch} />
              </Col>
            </Col>
          </Row>
          <Modal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.toggleModal}
            style={customStyles}
            ariaHideApp={false}
            contentLabel="Add Employee"
          >
            <div className="button-container">
              <button onClick={this.toggleModal} className="close-button">
                X
              </button>
            </div>
            <form className="add-employee">
              <li>
                <center>
                  <h2>Customer Details</h2>
                </center>
              </li>
              <li>
                <input
                  onChange={this.handleChange}
                  name="customer"
                  type="text"
                  // min="0000000000"
                  // max="9999999999"
                  pattern="[0-9]"
                  // maxlength="10"
                  className="input2"
                  placeholder="E-mail or Phone No."
                />
              </li>
              {/* <li>
                <input
                  onChange={this.handleChange}
                  name="phoneNo"
                  type="number"
                  className="input2"
                  placeholder="Phone Number"
                />
              </li> */}
              <li>
                <button
                  onClick={e => this.findCustomer(e)}
                  type="submit"
                  id="submit-landing2"
                >
                  Submit
                </button>
              </li>
            </form>
          </Modal>
          <RightMenu
            style={{ marginTop: "-10px" }}
            handleOk={(type, value) => this.handleOk(type, value)}
            cashGiven={cash => this.cashGiven(cash)}
            clickedCredit={this.creditCheck}
          />
        </Home>
      );
    }
  }
}
const mapStateToProps = state => {
  return {};
};
const Billing = connect(mapStateToProps)(BillingComponent);
export default Billing;
