import React, { Fragment } from 'react';
import { Card } from 'antd';
import PropTypes from 'prop-types';

const gridStyle = {
  width: '100%',
  textAlign: 'center'
};

const SingleCustomer = props => {
  console.log(props);
  let titlecontent = 'test';
  const { firstName, lastName, email, phoneNo } = props.customer;
  const { line1, line2, city, pin, state, country } = props.customer.address;
  return (
    <Fragment>
      <Card
        title={`${firstName} ${lastName}`}
        extra={`Email: ${email} Phone: ${phoneNo}`}
      >
        <Card.Grid style={gridStyle}>
          <h4>Address:</h4>
          <p>{`${line1} ${line2}`}</p>
          <p>{`${city} ${pin}`}</p>
          <p>{`${state} ${country}`}</p>
        </Card.Grid>
      </Card>
    </Fragment>
  );
};

export default SingleCustomer;
