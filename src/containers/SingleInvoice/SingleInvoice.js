import React, { Component, Fragment } from 'react';
import { Card, Table, Button, Avatar, notification } from 'antd';
import endpoint from '../../helpers/endpoint';

const { Meta } = Card;

const columns = [
  {
    title: 'Item No.',
    dataIndex: 'no'
  },
  {
    title: 'Name',
    dataIndex: 'name'
  },
  {
    title: 'OTY',
    dataIndex: 'quantity'
  },
  {
    title: 'Price',
    dataIndex: 'price'
  },
  {
    title: 'Amount',
    dataIndex: 'amount'
  }
];

const gridStyle = {
  flex: '1 0 50%',
  maxWidth: '50%',
  padding: '10px',
  boxShadow: 'none',
  border: 'none'
};

const fullWidth = {
  flex: '1 0 100%',
  boxShadow: 'none',
  border: 'none'
};

class SingleInvoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false
    };
  }

  markPaid = id => {
    const hdr = localStorage.token;
    fetch(`${endpoint}/invoice/${id}`, {
      method: 'PUT',
      headers: {
        'x-auth-token': hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === 'failure') {
            this.openNotification('error', result.message);
          } else {
            this.openNotification('success', result.message);
          }
        },
        error => {
          console.log(error);
          this.setState({
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  render() {
    console.log(this.props);
    const { data, branch } = this.props;
    const { customerId, isPaid, _id } = this.props.data;
    const { name, address, taxNo, phoneNo, email } = this.props.branch;

    const table = data.items;
    let tableData = table.map((item, index) => {
      return {
        no: index + 1,
        name: item.productName,
        quantity: item.quantity,
        price: `Kr.${item.productPrice}`,
        amount: `Kr.${item.quantity * item.productPrice}`
      };
    });
    return (
      <div
        style={{
          margin: '60px auto',
          display: 'flex',
          justifyContent: 'center'
        }}
        className="single-invoice"
      >
        <Card>
          <Meta
            avatar={
              <Avatar
                src={branch.logo === null ? '' : `${endpoint}/${branch.logo}`}
              />
            }
            title={name}
          />
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <Card.Grid style={gridStyle}>
              {address && (
                <>
                  <p>{address.line1}</p>
                  <p>{address.line2}</p>
                  <p>
                    {address.city}, {address.pin}, {address.state}
                  </p>
                  <p>Tax no.: {taxNo}</p>
                  <p>Phone: {phoneNo}</p>
                  <p>email: {email}</p>
                </>
              )}
            </Card.Grid>
            <Card.Grid style={{ ...gridStyle, textAlign: 'right' }}>
              {/* <img
                src={branch.logo === null ? '' : `${endpoint}/${branch.logo}`}
                style={{
                  maxHeight: '60px',
                  width: 'auto',
                  marginBottom: '1rem'
                }}
              /> */}
              <p>Invoice no: {data.invoiceNumber}</p>
              <p>Date: {data.createdDate}</p>
              <p>Time: {data.createdTime}</p>
              <p>Order Type: {data.orderType}</p>
            </Card.Grid>
            <Card.Grid style={gridStyle}>
              {customerId ? (
                <Fragment>
                  <p>
                    <strong>
                      {customerId.firstName} {customerId.lastName}
                    </strong>
                  </p>

                  <p>{customerId.address.line1}</p>
                  <p>{customerId.address.line2}</p>

                  {customerId.address.line1 ? (
                    <p>
                      {customerId.address.city}, {customerId.address.pin},{' '}
                      {customerId.address.state}
                    </p>
                  ) : (
                    ''
                  )}
                  <p>Phone: {customerId.phoneNo}</p>
                  <p>email: {customerId.email}</p>
                </Fragment>
              ) : (
                ''
              )}
            </Card.Grid>

            <Table
              style={{ ...fullWidth, margin: '10px 0' }}
              columns={columns}
              dataSource={tableData}
              size="middle"
              pagination={false}
            />
            <Card.Grid
              style={{
                ...fullWidth,
                textAlign: 'right',
                paddingRight: '0 40px',
                lineHeight: 1.65
              }}
            >
              <p>
                <span style={{ fontWeight: 700 }}>SubTotal: </span>
                <span style={{ minWidth: '100px', display: 'inline-block' }}>
                  {`Kr.${(
                    data.taxPrice -
                    (
                      data.taxPrice -
                      (100 * data.taxPrice) / (100 + data.tax)
                    ).toFixed(2)
                  ).toFixed(2)}`}
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 700 }}>
                  Tax(
                  {`${data.tax.toFixed(2)}%`}
                  ):{' '}
                </span>
                <span style={{ minWidth: '100px', display: 'inline-block' }}>
                  {`Kr.${(
                    data.taxPrice -
                    (100 * data.taxPrice) / (100 + data.tax)
                  ).toFixed(2)}`}
                </span>
              </p>
              <p>
                <span style={{ fontWeight: 700 }}>
                  Discount (
                  {data.discountType === 'percent' ? `${data.discount}%` : '0%'}
                  ) :
                </span>
                <span style={{ minWidth: '100px', display: 'inline-block' }}>
                  {/* {data.discountType === 'percent'
                      ? `- Kr.${(100 * data.taxPrice) / (100 - data.discount)}`
                      : `- Kr.
                    ${data.discount ? data.discount.toFixed(2) : 0}`} */}
                  {`- Kr.${(
                    (100 * Number(data.taxPrice)) / (100 - data.discount) -
                    data.taxPrice
                  ).toFixed(2)}`}
                </span>
              </p>
              <span style={{ fontWeight: 700 }}>Total:</span>
              <span style={{ minWidth: '100px', display: 'inline-block' }}>
                {`Kr.${data.taxPrice}`}
              </span>
              <br />
              <span>
                {isPaid ? (
                  ''
                ) : (
                  <Button
                    style={{ margin: '20px 0' }}
                    type="danger"
                    onClick={() => this.markPaid(_id)}
                  >
                    Mark as Paid
                  </Button>
                )}
              </span>
            </Card.Grid>
          </div>
        </Card>
      </div>
    );
  }
}

export default SingleInvoice;
