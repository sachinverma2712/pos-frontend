import React, { Component, Fragment } from "react";
import Home from "../Home/Home";
import "./stockDetails.css";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import {
  Table,
  Input,
  InputNumber,
  Popover,
  Form,
  Button,
  notification
} from "antd";
import endpoint from "../../helpers/endpoint";

const FormItem = Form.Item;
const EditableContext = React.createContext();

const EditableRow = ({ form, index, ...props }) => (
  <EditableContext.Provider value={form}>
    <tr {...props} />
  </EditableContext.Provider>
);

const EditableFormRow = Form.create()(EditableRow);

class EditableCell extends Component {
  getInput = () => {
    if (this.props.inputType === "number") {
      return <InputNumber />;
    }
    return <Input />;
  };

  render() {
    const {
      editing,
      dataIndex,
      title,
      inputType,
      record,
      index,
      ...restProps
    } = this.props;
    return (
      <EditableContext.Consumer>
        {form => {
          const { getFieldDecorator } = form;
          return (
            <td {...restProps}>
              {editing ? (
                <FormItem style={{ margin: 0 }}>
                  {getFieldDecorator(dataIndex, {
                    rules: [
                      {
                        required: true,
                        message: `Please Input ${title}!`
                      }
                    ],
                    initialValue: record[dataIndex]
                  })(this.getInput())}
                </FormItem>
              ) : (
                restProps.children
              )}
            </td>
          );
        }}
      </EditableContext.Consumer>
    );
  }
}

class stockDetailsComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      isLoaded: false,
      category: true,
      visiblePopover: false,
      visibleEditPopover: false,
      editingKey: "",
      popoverId: "",
      deletingKey: "",
      items: [],
      PaginationNo: 1
    };

    this.stockCatColumns = [
      {
        title: "Sr. No.",
        key: "index",
        render: (text, record, index) =>
          //? for correct sr no., check antd pagination and table/pagination docs
          //? index + (currentpage - 1)*10
          {
            return index + (this.state.PaginationNo - 1) * 10 + 1;
          }
      },
      {
        title: "Category Name",
        key: "name",
        dataIndex: "name",
        editable: true,
        render: (text, record, index) => (
          <a
            onClick={() =>
              this.handleClick(
                //? for correct sr no., check antd pagination and table/pagination docs
                //? index + (currentpage - 1)*10
                this.state.items[index + (this.state.PaginationNo - 1) * 10]._id
              )
            }
          >
            {text}
          </a>
        )
      },
      {
        title: "Edit",
        dataIndex: "operation",
        width: "25%",
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        onClick={() => this.save(form, record._id)}
                        style={{ marginRight: 8 }}
                      >
                        Save
                      </a>
                    )}
                  </EditableContext.Consumer>
                  <Popover
                    content={
                      <span
                        style={{
                          display: "flex",
                          justifyContent: "center"
                        }}
                      >
                        <a
                          style={{ color: "#F44336" }}
                          onClick={() => this.cancel(record.name)}
                        >
                          Cancel
                        </a>
                      </span>
                    }
                    title="Are you Sure?"
                    trigger="click"
                    visible={this.state.visibleEditPopover}
                    onVisibleChange={this.handleEditVisibleChange}
                  >
                    <a
                      style={{ color: "#1890ff" }}
                      onClick={this.openEditPopover}
                    >
                      Cancel
                    </a>
                  </Popover>
                </span>
              ) : (
                <a
                  style={{ color: "#1890ff" }}
                  onClick={() => this.edit(record._id)}
                >
                  Edit
                </a>
              )}
            </div>
          );
        }
      },
      {
        title: "Delete",
        width: "15%",
        // key: 'action',
        render: (text, record) => {
          const deletable = this.isDeleting(record);
          return (
            <Fragment>
              {deletable ? (
                <Popover
                  content={
                    <span
                      style={{
                        display: "flex",
                        justifyContent: "space-around"
                      }}
                    >
                      <a style={{ color: "#1890ff" }} onClick={this.hideDelete}>
                        Cancel
                      </a>
                      <a
                        style={{ color: "#F44336" }}
                        onClick={e => this.delete(e, record)}
                      >
                        Delete
                      </a>
                    </span>
                  }
                  title="Are you Sure?"
                  trigger="click"
                  visible={true}
                  onVisibleChange={this.handleVisibleChange}
                >
                  <a
                    onClick={() => this.setState({ popoverId: record.id })}
                    style={{ color: "#F44336" }}
                  >
                    Delete
                  </a>
                </Popover>
              ) : (
                <a
                  onClick={() => this.deleteTemp(record._id)}
                  style={{ color: "#F44336" }}
                >
                  Delete
                </a>
              )}
            </Fragment>
          );
        }
      }
    ];

    this.stockProdColumns = [
      {
        title: "Sr. No.",
        key: "index",
        render: (text, record, index) =>
          //? for correct sr no., check antd pagination and table/pagination docs
          //? index + (currentpage - 1)*10
          {
            return index + (this.state.PaginationNo - 1) * 10 + 1;
          }
      },
      {
        title: "Product Name",
        key: "name",
        dataIndex: "name",
        editable: true,
        render: (text, record, index) => (
          <a
            onClick={() =>
              this.handleClick(
                //? for correct sr no., check antd pagination and table/pagination docs
                //? index + (currentpage - 1)*10
                this.state.items[index + (this.state.PaginationNo - 1) * 10]._id
              )
            }
          >
            {text}
          </a>
        )
      },
      {
        title: "Quantity",
        key: "quantity",
        dataIndex: "quantity"
      },
      {
        title: "Edit",
        dataIndex: "operation",
        width: "25%",
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <a
                        onClick={() => this.save(form, record._id)}
                        style={{ marginRight: 8 }}
                      >
                        Save
                      </a>
                    )}
                  </EditableContext.Consumer>
                  <Popover
                    content={
                      <span
                        style={{
                          display: "flex",
                          justifyContent: "center"
                        }}
                      >
                        <a
                          style={{ color: "#F44336" }}
                          onClick={() => this.cancel(record.name)}
                        >
                          Cancel
                        </a>
                      </span>
                    }
                    title="Are you Sure?"
                    trigger="click"
                    visible={this.state.visibleEditPopover}
                    onVisibleChange={this.handleEditVisibleChange}
                  >
                    <a
                      style={{ color: "#1890ff" }}
                      onClick={this.openEditPopover}
                    >
                      Cancel
                    </a>
                  </Popover>
                </span>
              ) : (
                <a onClick={() => this.edit(record._id)}>Edit</a>
              )}
            </div>
          );
        }
      },
      {
        title: "Delete",
        width: "15%",
        // key: 'action',
        render: (text, record) => {
          const deletable = this.isDeleting(record);
          return (
            <Fragment>
              {deletable ? (
                <Popover
                  content={
                    <span
                      style={{
                        display: "flex",
                        justifyContent: "space-around"
                      }}
                    >
                      <a style={{ color: "#1890ff" }} onClick={this.hideDelete}>
                        Cancel
                      </a>
                      <a
                        style={{ color: "#F44336" }}
                        onClick={e => this.delete(e, record)}
                      >
                        Delete
                      </a>
                    </span>
                  }
                  title="Are you Sure?"
                  trigger="click"
                  visible={true}
                  onVisibleChange={this.handleVisibleChange}
                >
                  <a
                    onClick={() => this.setState({ popoverId: record.id })}
                    style={{ color: "#F44336" }}
                  >
                    Delete
                  </a>
                </Popover>
              ) : (
                <a
                  onClick={() => this.deleteTemp(record._id)}
                  style={{ color: "#F44336" }}
                >
                  Delete
                </a>
              )}
            </Fragment>
          );
        }
      }
    ];
  }

  edit(key) {
    this.setState({ editingKey: key });
  }

  handleVisibleChange = () => {
    this.setState({ visiblePopover: true });
  };

  hideDelete = () => {
    this.setState({ deletingKey: "" });
  };

  save(form, key) {
    form.validateFields((error, row) => {
      if (error) {
        return;
      }
      const newData = [...this.state.items];
      const index = newData.findIndex(item => key === item._id);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row
        });

        this.setState(
          {
            items: newData,
            editingKey: ""
          },
          this.state.category
            ? this.updateCategory(row.name, item._id)
            : this.updateProduct(row.name, item._id)
        );
      } else {
        newData.push(row);
        this.setState({ items: newData, editingKey: "" });
      }
    });
  }

  delete(e, record) {
    e.preventDefault();
    const newData = [...this.state.items];
    const index = newData.findIndex(item => record._id === item._id);
    if (index > -1) {
      const item = newData[index];
      newData.splice(index, 1);
      this.setState(
        {
          items: newData,
          editingId: item._id
        },
        this.state.category
          ? this.deleteCategory(item._id)
          : this.deleteProduct(item._id)
      );
    } else {
      newData.push(record);
      this.setState({ items: newData, editingKey: "" });
    }
  }

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  goToCat = () => {
    const { category } = this.state;
    if (!category) {
      this.setState({ category: true, isLoaded: false });
      this.categoryPage();
    } else {
      this.navigate("/stock");
    }
  };

  deleteTemp(key) {
    this.setState({ deletingKey: key });
  }

  openEditPopover = () => {
    this.setState({ visibleEditPopover: true });
  };

  isEditing = record => {
    return record._id === this.state.editingKey;
  };

  isDeleting = record => {
    return record._id === this.state.deletingKey;
  };

  handleClick = id => {
    if (this.state.category) {
      this.setState({ isLoaded: false });
      this.productPage(id);
    } else {
      console.log("beep bop");
    }
  };

  productPage = id => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/stock/product/${id}`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              isLoaded: true,
              items: result.data,
              category: false,
              catId: id,
              newItem: ""
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  categoryPage = () => {
    var hdr = localStorage.token;
    fetch(`${endpoint}/stock`, {
      method: "GET",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.setState({
              isLoaded: true,
              items: result.data.reverse()
            });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  handleAddCategory = e => {
    e.preventDefault();
    const { newItem } = this.state;
    var hdr = localStorage.token;
    var data = JSON.stringify({ name: newItem });
    fetch(`${endpoint}/stock`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.categoryPage();
            this.openNotification("success", "Category added successfully.");
            window.location.reload();
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  handleAddProduct = () => {
    const { newItem, catId } = this.state;

    var hdr = localStorage.token;
    var data = JSON.stringify({ name: newItem });
    fetch(`${endpoint}/stock/product/${catId}`, {
      method: "POST",
      body: data,
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.productPage(catId);
            this.openNotification("success", "Product added successfully.");
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  updateCategory = (name, id) => {
    var hdr = localStorage.token;
    var data = JSON.stringify({ name });
    console.log(data);
    fetch(`${endpoint}/stock/${id}`, {
      method: "PUT",
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      },
      body: data
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.openNotification("success", "Category updated successfully.");
            // this.setState({
            //   isLoaded: true,
            //   items: result.data
            // });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  updateProduct = (name, id) => {
    var hdr = localStorage.token;
    var data = JSON.stringify({ name });
    console.log(data);
    fetch(`${endpoint}/stockProduct/${id}`, {
      method: "PUT",
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      },
      body: data
    })
      .then(res => res.json())
      .then(
        result => {
          if (result.status === "failure") {
            this.openNotification("error", result.data);
          } else {
            this.openNotification("success", "Product updated successfully.");
            // this.setState({
            //   isLoaded: true,
            //   items: result.data
            // });
          }
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        error => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      );
  };

  deleteCategory = id => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/stock/${id}`, {
      method: "DELETE",
      headers: {
        "x-auth-token": hdr,
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification("error", result.data);
        } else {
          this.openNotification("success", "Category deleted successfully.");
          console.log(result.message);
        }
      });
  };

  deleteProduct = id => {
    let hdr = localStorage.token;
    fetch(`${endpoint}/stockProduct/${id}`, {
      method: "DELETE",
      headers: {
        "x-auth-token": hdr
      }
    })
      .then(res => res.json())
      .then(result => {
        if (result.status === "failure") {
          this.openNotification("error", result.data);
        } else {
          this.openNotification("success", "Product deleted successfully.");
          console.log(result.message);
        }
      });
  };

  componentDidMount() {
    this.categoryPage();
  }

  openNotification = (type, message) => {
    notification[type]({
      message: message
    });
  };

  render() {
    console.log(this.state);

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell
      }
    };

    const stockCatColumns = this.stockCatColumns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === "price" ? "number" : "text",
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });

    const stockProdColumns = this.stockProdColumns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === "quantity" ? "number" : "text",
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });
    const { error, isLoaded, category } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else {
      return (
        <Home isAdmin={true}>
          <div className="report-container">
            <span className="item">Stock</span>
            <span className="item">
              <span id="less-visible">HOME / </span>
              Stock List
            </span>
            <span className="item">
              <span id="less-visible">
                {
                  <div onClick={this.goToCat} className="back-button-border">
                    <i className="fa fa-arrow-circle-left" aria-hidden="true" />
                    <span>Back</span>
                  </div>
                }
              </span>
            </span>
          </div>
          {category ? (
            <Fragment>
              <Form onSubmit={this.handleAddCategory} required>
                <Input
                  style={{ width: 200, "margin-right": "10px" }}
                  compact={true}
                  type="text"
                  onChange={e => this.setState({ newItem: e.target.value })}
                  placeholder="Category Name"
                  value={this.state.newItem}
                  required
                />
                <Button
                  type="primary"
                  style={{ marginBottom: 16 }}
                  onClick={this.handleAddCategory}
                >
                  Add a Category
                </Button>
              </Form>
              <Table
                rowKey={record => record.name}
                loading={isLoaded ? false : true}
                onChange={pagination => this.pageChange(pagination)}
                components={components}
                bordered
                dataSource={this.state.items}
                columns={stockCatColumns}
                rowClassName="editable-row"
              />
            </Fragment>
          ) : (
            <Fragment>
              <Form>
                <Input
                  style={{ width: 200, "margin-right": "10px" }}
                  compact={true}
                  onChange={e => this.setState({ newItem: e.target.value })}
                  placeholder="Product Name"
                  value={this.state.newItem}
                />
                <Button
                  onClick={this.handleAddProduct}
                  type="primary"
                  style={{ marginBottom: 16 }}
                >
                  Add a Product
                </Button>
              </Form>
              <Table
                rowKey={record => record.name}
                loading={isLoaded ? false : true}
                onChange={pagination => this.pageChange(pagination)}
                components={components}
                bordered
                dataSource={this.state.items}
                columns={stockProdColumns}
                rowClassName="editable-row"
              />
            </Fragment>
          )}
        </Home>
      );
    }
  }

  navigate = route => {
    const { dispatch } = this.props;
    dispatch(push(route));
  };
}
const mapStateToProps = state => {
  return {};
};

const StockDetails = connect(mapStateToProps)(stockDetailsComponent);

export default StockDetails;
